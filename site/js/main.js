window.onload = function(){
    app.nbPage = 4;
    app.start();
};

$(document).ready(function() {
		$('.js-scrollTo').on('click', function() {
			var page = $(this).attr('href');
			var speed = 800;
			$('html, body').animate( { scrollTop: $(page).offset().top }, speed );
			return false;
		});
        var newsContainer = document.getElementById("news-container");
        $.getJSON("news.json",function(data){
         $( data.news ).each(function (i) {
              var a = document.createElement("a");
              a.setAttribute("href", this.lien);
              a.setAttribute("class", "news-item");
              newsContainer.appendChild(a);
              var h3 = document.createElement("h3");
              h3.setAttribute("class", "news-item-title");
              h3.textContent = this.titre;
              a.appendChild(h3);
              var p = document.createElement("p");
              p.setAttribute("class", "news-item-excerpt");
              p.textContent = this.extrait;
              a.appendChild(p);
              var span = document.createElement("span");
              span.setAttribute("class", "news-item-date");
              span.textContent = this.date;
              a.appendChild(span);
          });
        });
	});

function myPage(){
    var me = this;
    var sizeOfPage;
    var nbPage;
    var heightPage;

    this.start = function(){
        console.log("Starting myPage Instance");
        // On ajoute l'event et la location a chaque LI
        $(".smoothScroll").click(function(){
            var el = $(this);
            (function(){
                me.changeLocation(el.attr("rel"));
            }());
        });

        $(document).scroll(function(){
            me.onScroll();
        });

        me.sizeOfPage = Math.max($(document).height(), $(window).height());
        me.heightPage = window.innerHeight;
    };

    this.changeLocation = function(location){
        $("html , body").animate({
            scrollTop : $("#" + location).offset().top
        },500);
    };

    this.onScroll = function(){
        var offsetX = document.body.scrollTop;
        var idPage = Math.round(offsetX / me.heightPage);
        $(".ctn-img:eq("+idPage+") .ctn-img-img").css("background-position", "50%"+ (((-offsetX - (idPage * me.heightPage)) /2)-25) + "px");
        if (idPage === 0) {
            $(".navbar ul").removeClass('scrolled');
        }else{
            $(".navbar ul").addClass('scrolled');
        }

       // $("ul li.active").removeClass("active");
        //$("ul li:eq("+idPage+")").addClass("active");
    };
}

var app = new myPage();
