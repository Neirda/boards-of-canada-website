
/**
* Animation du Slider
*/
var rotation = 45;

$('.wrap').css('-webkit-transform','rotate('+rotation+'deg)');

$('#next').click(function(){
    rotation += 90;
    $('.wrap').css(
        '-webkit-transform', 'rotate('+rotation+'deg)'
    );
    $('#playButton').removeClass('hidden');
    $('#stopButton').addClass('hidden');
});

$('#prev').click(function(){
    rotation -= 90;
    $('#playButton').removeClass('hidden');
    $('#stopButton').addClass('hidden');
    $('.wrap').css(
        '-webkit-transform', 'rotate('+rotation+'deg)'
    );
});

/**
* Bouton play/stop
*/
$('#playButton').click(function(){
    $('#playButton').addClass('hidden');
    $('#stopButton').removeClass('hidden');
    console.log("play");
});
$('#stopButton').click(function(){
    $('#playButton').toggleClass('hidden');
    $('#stopButton').toggleClass('hidden');
    console.log("stop");
});
