
(function() {
    var AudioContext;
    var audio;
    var audioContext;
    var source;
    var analyser;

    var canvas = document.getElementById("theCanvas");
    var canvasContext = canvas.getContext("2d");
    var dataArray;
    var analyserMethod = "getByteTimeDomainData";
    var slider = document.getElementById("slider1");
    var streamUrl;
    var isIdle = true;

    var canvasWidth = canvas.width;
    var canvasHeight = canvas.height;

    var i=0;
    var tabSongs = ['https://soundcloud.com/skywurdz/squeak-squeak',
    'https://soundcloud.com/skywurdz/soulgroove',
    'https://soundcloud.com/skywurdz/half-vast-1',
    'https://soundcloud.com/skywurdz/fcuk-yesterday-7'];



    function initAudio(streamUrl) {
        AudioContext = window.AudioContext || window.webkitAudioContext;
        audio = new Audio();
        audio.crossOrigin = "anonymous";
        audioContext = new AudioContext();
        source = audioContext.createMediaElementSource(audio);
        source.connect(audioContext.destination);
        analyser = audioContext.createAnalyser();
        source.connect(analyser);
    }

    function get(url, callback) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState === 4 && request.status === 200) {
                callback(request.responseText);
            }
        };

        request.open("GET", url, true);
        request.send(null);
    }

    var clientParameter = "client_id=3b2585ef4a5eff04935abe84aad5f3f3"

    // Basing everything on the track's permalink URL. That is what the user knows.
    // Makes it possible to use the text box for pasting any track URL.

    var trackPermalinkUrl;
    function findTrack() {
        trackPermalinkUrl = tabSongs[i];
        get("https://api.soundcloud.com/resolve.json?url=" +  trackPermalinkUrl + "&" + clientParameter,
        function (response) {
            var trackInfo = JSON.parse(response);
            slider.max = trackInfo.duration / 1000;
            document.getElementById("totalTime").innerHTML = millisecondsToHuman(trackInfo.duration);
            document.getElementById("artistUrl").href = trackInfo.user.permalink_url;
            //document.getElementById("artistAvatar").src = trackInfo.user.avatar_url;
            document.getElementById("artistName").innerHTML = "Boards Of Canada"; //trackInfo.user.username
            document.getElementById("trackUrl").href = trackInfo.permalink_url[18,trackInfo.permalink_url.length];
            /*if(trackInfo.artwork_url) {
            document.getElementById("trackArt").src = trackInfo.artwork_url;
        } else {
        document.getElementById("trackArt").src = "";
    }*/
    document.getElementById("trackName").innerHTML = trackInfo.title;
    streamUrl = trackInfo.stream_url + "?" + clientParameter;
}
);
};


function startMusic() {
    audio.src = streamUrl;
    audio.play();
    slider.value = 0;
    // Using four seconds so the user can change the value of
    // the slider. Too short interval will cause the automatic
    // updating to steal the control from the user.
    setInterval(function () {
        slider.value = audio.currentTime;
    }, 4000);

    var currentTime = document.getElementById("currentTime");
    setInterval(function () {
        currentTime.innerHTML = millisecondsToHuman(audio.currentTime * 1000);
    }, 1000);

    //startDrawing();
}
function PlayNext() {

    i=i+1;
    if(i>3)
    {
        i=0;
    }
    console.log(i);
    stopMusic();
    findTrack();
    initAudio();
}

function PlayPrev() {

    i=i-1;
    if(i<0)
    {
        i=3;
    }
    console.log(i);
    stopMusic();
    findTrack();
    initAudio();
}

function jumpTo(here) {
    if (!audio.readyState) return false;
    audio.currentTime = here;
    //}
    //audio.fastSeek(here);
};

slider.addEventListener("change", function () {
    jumpTo(this.value);
});

function millisecondsToHuman(milliseconds) {
    var date = new Date(null);
    date.setMilliseconds(milliseconds);
    return date.toISOString().substr(11, 8);
};

function stopMusic() {

    audio.pause();
    /*var currentTime = document.getElementById("currentTime");
    setInterval(function () {
    currentTime.innerHTML = millisecondsToHuman( 0* 1000);
}, 0);	currentTime=0;
slider.value = 0;*/

}





document.getElementById("playButton").addEventListener("click", startMusic);
document.getElementById("stopButton").addEventListener("click", stopMusic);

document.getElementById("next").addEventListener("click", PlayNext);
document.getElementById("prev").addEventListener("click", PlayPrev);

/*document.getElementById("oscilloscopeButton").addEventListener("click", function(){
analyserMethod = "getByteTimeDomainData";
startDrawing();
});

document.getElementById("frequencyBarsButton").addEventListener("click", function(){
analyserMethod = "getByteFrequencyData";
startDrawing();
});*/



// startIdleAnimation();
findTrack();
initAudio();

})();
