Note à l’intention de Julien : 

Tu observeras qu’il y’a un dossier site/ et un dossier site_version_sans_JSON/


Cela est du à un problème de « cross-origin request » que tu dois bien connaitre… 
Aussi, si tu veux tester la version normale, il te faudra l’ouvrir sur un serveur (wamp, mamp…)

La version sans JSON est simplement ici pour qu’on puisse avoir un aperçu du site sans avoir à le mettre sur un serveur.

Have Fun.

