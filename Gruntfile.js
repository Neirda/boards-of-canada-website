module.exports = function(grunt) {
    grunt.initConfig({
        less : {
            development: {
                options: {
                    paths: ['less/']
                },
                files: {
                    'site/css/style.css': 'less/*.less'
                }
            }
        },
        cssmin: {
            target: {
                files: {
                    'site/css/min.css': ['site/css/style.css']
                }
            }
        },
        watch: {
            css: {
                files: ['less/*.less'],
                tasks: ['default'],
                options: {
                    spawn: false,
                }
            }
        },
        uglify: {
            my_target: {
                files: {
                    'site/js/min.js': ['site/js/**/*.js']
                }
            }
        }
    });


    // grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-csslint');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('default', ['less','cssmin','uglify']);

};
